<?php
/**
 * Template Name: Especial Febrero
 *
 * @package WordPress
 */
?>

<?php get_header('febrero'); ?>
<?php 
if (have_posts()) :
   while (have_posts()) :
      the_post();
         the_content();
   endwhile;
endif;
 ?>

<!--// WordPress Hook //-->
<?php get_footer(); ?>
