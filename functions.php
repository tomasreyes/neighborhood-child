<?php

/**
 * Add sku, author, publisher and format to product search
 */
 
// hook into wp pre_get_posts
add_action('pre_get_posts', 'jc_woo_search_pre_get_posts');
 
/**
 * Add custom join and where statements to product search query
 * @param  mixed $q query object
 * @return void
 */
function jc_woo_search_pre_get_posts($q){
 
    if ( is_search() ) {
        add_filter( 'posts_join', 'jc_search_post_join' );
        add_filter( 'posts_where', 'jc_search_post_excerpt' );
    }
}
 
/**
 * Add Custom Join Code for wp_mostmeta table
 * @param  string $join
 * @return string
 */
function jc_search_post_join($join = ''){
 
    global $wp_the_query;
 
    // escape if not woocommerce searcg query
    if ( empty( $wp_the_query->query_vars['wc_query'] ) || empty( $wp_the_query->query_vars['s'] ) )
            return $join;
 
    $join .= "INNER JOIN wp_postmeta AS jcmt1 ON (wp_posts.ID = jcmt1.post_id)";
    return $join;
}
 
/**
 * Add custom where statement to product search query
 * @param  string $where
 * @return string
 */
function jc_search_post_excerpt($where = ''){
 
    global $wp_the_query;
 
    // escape if not woocommerce search query
    if ( empty( $wp_the_query->query_vars['wc_query'] ) || empty( $wp_the_query->query_vars['s'] ) )
            return $where;
 
    $where = preg_replace("/post_title LIKE ('%[^%]+%')/", "post_title LIKE $1)
                OR (jcmt1.meta_key = '_sku' AND CAST(jcmt1.meta_value AS CHAR) LIKE $1)
                OR  (jcmt1.meta_key = '_author' AND CAST(jcmt1.meta_value AS CHAR) LIKE $1)
                OR  (jcmt1.meta_key = '_publisher' AND CAST(jcmt1.meta_value AS CHAR) LIKE $1)
                OR  (jcmt1.meta_key = '_format' AND CAST(jcmt1.meta_value AS CHAR) LIKE $1 ", $where);
 
    return $where;
}







	add_action('get_header', 'remove_admin_login_header');
		function remove_admin_login_header() {
			remove_action('wp_head', '_admin_bar_bump_cb');
	}

	/* VINCULO CON THEME PARENT
	================================================== */
	add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
	function my_theme_enqueue_styles() {
		wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
	}

	/* SOPORTE DE UPLOAD DE SVGS
	================================================== */
	function cc_mime_types($mimes) {
	  $mimes['svg'] = 'image/svg+xml';
	  return $mimes;
	}
	add_filter('upload_mimes', 'cc_mime_types');



	/* action para agregar shortcodes*/
	add_action( 'init', 'add_utils_shortcodes' );

	function add_utils_shortcodes() {

		add_shortcode( 'my-login-form', 'login_form_blank' );
	}

	function login_form_blank($attr) {
		//if ( is_user_logged_in() )
			//return '';

		/* Set up some defaults. */
		$defaults = array(
			'redirect' => '/recicla-con-estilo-gracias',
			'form_id' => 'login-campana',
			'label_username' => 'Username',
			'label_password' => 'Password',
			'label_remember' => '',
			'label_log_in' => '¡Quiero Participar!',
			'remember' => false
		);

		/* Merge the user input arguments with the defaults. */
		$attr = shortcode_atts( $defaults, $attr );

		/* Set 'echo' to 'false' because we want it to always return instead of print for shortcodes. */
		$attr['echo'] = false;

		return wp_login_form( $attr );
	}


	//https://pippinsplugins.com/creating-custom-front-end-registration-and-login-forms/
	// user registration login form
	function denda_registration_form() {

		// only show the registration form to non-logged-in members
		//if(!is_user_logged_in()) {

			global $pippin_load_css;

			// set this to true so the CSS is loaded
			$pippin_load_css = true;

			// check to make sure user registration is enabled
			$registration_enabled = get_option('users_can_register');

			// only show the registration form if allowed
			if($registration_enabled) {
				$output = denda_registration_form_fields();
			} else {
				$output = __('User registration is not enabled');
			}
			return $output;
		//}
	}
	add_shortcode('denda_register_form', 'denda_registration_form');


	// registration form fields
	function denda_registration_form_fields() {

		ob_start(); ?>

	 		<?php
			// show any error messages after form submission
			denda_show_error_messages(); ?>

	 		<form id="login-campana" class="denda_form" action="" method="POST">

	 			<p class="login-username">
	 				<label for="denda_user_email">Correo electrónico</label><br>
	 				<input type="text" name="denda_user_email" id="user_login" class="input" value="" size="20"></p>
	 				<p class="login-password">
	 				<label for="denda_user_pass">Contraseña</label><br>
	 			<input type="password" name="denda_user_pass" id="user_pass" class="input" value="" size="20"></p>
	 			<p class="login-submit">
				<p>
					<input type="hidden" name="denda_register_nonce" value="<?php echo wp_create_nonce('denda-register-nonce'); ?>"/>
					<input type="submit" name="wp-submit" id="wp-submit" class="button-primary" value="¡Quiero Participar!"><br>
				</p>

	 			<!--<input type="hidden" name="redirect_to" value="/recicla-con-estilo-gracias"></p>-->

 			</form>


		<?php
		return ob_get_clean();
	}


	// register a new user
	function denda_add_new_member() {

	  	if (isset( $_POST["denda_user_email"] ) && wp_verify_nonce($_POST['denda_register_nonce'], 'denda-register-nonce')) {




			$user_login		= $_POST["denda_user_email"];
			$user_email		= $_POST["denda_user_email"];
			//$user_first 	= $_POST["pippin_user_first"];
			//$user_last	 	= $_POST["pippin_user_last"];
			$user_pass		= $_POST["denda_user_pass"];
			//$pass_confirm 	= $_POST["pippin_user_pass_confirm"];

			// this is required for username checks
			//require_once(ABSPATH . WPINC . '/registration.php');



			if(username_exists($user_login)) {
				// Username already registered
				denda_errors()->add('username_unavailable', __('Username already taken'));
			}

			if(!validate_username($user_login)) {
				// invalid username
				denda_errors()->add('username_invalid', __('Invalid username'));
			}

			if($user_login == '') {
				// empty username
				denda_errors()->add('username_empty', __('Please enter a username'));
			}
			if(!is_email($user_email)) {
				//invalid email
				denda_errors()->add('email_invalid', __('Invalid email'));
			}
			if(email_exists($user_email)) {
				//Email address already registered
				denda_errors()->add('email_used', __('Email already registered'));
			}
			if($user_pass == '') {
				// passwords do not match
				denda_errors()->add('password_empty', __('Please enter a password'));
			}
			/*
			if($user_pass != $pass_confirm) {
				// passwords do not match
				denda_errors()->add('password_mismatch', __('Passwords do not match'));
			}
			*/


			$errors = denda_errors()->get_error_messages();

			//print_r($errors);



			// only create the user in if there are no errors
			if(empty($errors)) {

				$new_user_id = wp_insert_user(array(
						'user_login'		=> $user_login,
						'user_pass'	 		=> $user_pass,
						'user_email'		=> $user_email,
						//'first_name'		=> $user_first,
						//'last_name'			=> $user_last,
						'user_registered'	=> date('Y-m-d H:i:s'),
						'role'				=> 'customer'
					)
				);
				if($new_user_id) {
					// send an email to the admin alerting them of the registration
					//wp_new_user_notification($new_user_id);

					// log the new user in
					//wp_setcookie($user_login, $user_pass, true);
					//wp_set_current_user($new_user_id, $user_login);
					//do_action('wp_login', $user_login);

					// send the newly created user to the home page after logging them in
					//wp_redirect('/recicla-con-estilo-gracias'); exit;
				}

			}

		}
	}
	add_action('init', 'denda_add_new_member');



	// displays error messages from form submissions
	function denda_show_error_messages() {
		if($codes = denda_errors()->get_error_codes()) {
			echo '<div class="pippin_errors">';
			    // Loop error codes and display errors
			   foreach($codes as $code){
			        $message = denda_errors()->get_error_message($code);
			        echo '<span class="error"><strong>' . __('Error') . '</strong>: ' . $message . '</span><br/>';
			    }
			echo '</div>';
		}
	}

	// used for tracking error messages
	function denda_errors(){
	    static $wp_error; // Will hold global variable safely
	    return isset($wp_error) ? $wp_error : ($wp_error = new WP_Error(null, null, null));
	}


	/******************** DENDA ******************/

	/**
	* filter translations, to replace some WooCommerce text with our own
	* @param string $translation the translated text
	* @param string $text the text before translation
	* @param string $domain the gettext domain for translation
	* @return string
	*/
	function wpse_77783_woo_bacs_ibn($translation, $text, $domain) {
	    if ($domain == 'woocommerce') {
	        switch ($text) {
	            case 'IBAN':
	                $translation = 'RUT';
	                break;

	            case 'BIC':
	                $translation = 'Correo';
	                break;
	        }
	    }

	    return $translation;
	}

	add_filter('gettext', 'wpse_77783_woo_bacs_ibn', 10, 3);



	// Disable for specific role (in this case, 'subscriber')
	add_action('after_setup_theme', 'remove_admin_bar');

	function remove_admin_bar() {
		show_admin_bar(false);

		// show admin bar only for admins
		if (!current_user_can('manage_options')) {
			show_admin_bar( false );
		}
		// show admin bar only for admins and editors
		if (!current_user_can('edit_posts')) {
			show_admin_bar( false );
		}
	}

	/***** FUNCION DE BITLY*********************/
	/* returns the shortened url */
	function get_bitly_short_url($url,$login,$appkey,$format='txt') {
		$connectURL = 'http://api.bit.ly/v3/shorten?login='.$login.'&apiKey='.$appkey.'&uri='.urlencode($url).'&format='.$format;
		return curl_get_result($connectURL);
	}

	/* returns expanded url */
	function get_bitly_long_url($url,$login,$appkey,$format='txt') {
		$connectURL = 'http://api.bit.ly/v3/expand?login='.$login.'&apiKey='.$appkey.'&shortUrl='.urlencode($url).'&format='.$format;
		return curl_get_result($connectURL);
	}

	/* returns a result form url */
	function curl_get_result($url) {
		$ch = curl_init();
		$timeout = 5;
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
		$data = curl_exec($ch);
		curl_close($ch);
		return $data;
	}

	function wc_ninja_remove_password_strength() {
	if ( wp_script_is( 'wc-password-strength-meter', 'enqueued' ) ) {
		wp_dequeue_script( 'wc-password-strength-meter' );
		}
	}
	add_action( 'wp_print_scripts', 'wc_ninja_remove_password_strength', 100 );


	// After registration, logout the user and redirect to home page
	function custom_registration_redirect() {
	    wp_logout();
	    return home_url('/');
	}
	//add_action('woocommerce_registration_redirect', 'custom_registration_redirect', 2);


	/**
 * WooCommerce product data tab definition
 *
 * @return array
 */

add_action('wc_cpdf_init', 'prefix_custom_product_data_tab_init', 10, 0);
if(!function_exists('prefix_custom_product_data_tab_init')) :

   function prefix_custom_product_data_tab_init(){


     $custom_product_data_fields = array();


     /** First product data tab starts **/
     /** ===================================== */

     $custom_product_data_fields['custom_data_1'] = array(

       array(
             'tab_name'    => __('Adicionales', 'wc_cpdf'),
       ),

       array(
             'id'          => '_marca',
             'type'        => 'text',
             'label'       => __('Marca', 'wc_cpdf'),
             'placeholder' => __('Coloca la marca del producto aquí.', 'wc_cpdf'),
             'class'       => 'large',
             'description' => __('Marca del producto.', 'wc_cpdf'),
             'desc_tip'    => true,
       ),
     );

     /** First product data tab ends **/
     /** ===================================== */



     return $custom_product_data_fields;

   }

endif;
	/*USO get the short url
	$short_url = get_bitly_short_url('https://dendamarke.com/','dendamarket','fdb2c2aa596acb0994e86e22a8ca9c4498f0e288');

	/* get the long url from the short one
	$long_url = get_bitly_long_url($short_url,'dendamarket','fdb2c2aa596acb0994e86e22a8ca9c4498f0e288');*/

	function patricks_billing_fields( $fields ) {
		global $woocommerce;

		// return the regular billing fields if we need shipping fields
		//echo $woocommerce->shipping->shipping_total;
		$chosen_methods = WC()->session->get( 'chosen_shipping_methods' );
 		$chosen_shipping = $chosen_methods[0];
 		//echo $chosen_shipping;
		if ( $chosen_shipping != 'local_pickup:3' ) {
			return $fields;
		}
	  // we don't need the billing fields so empty all of them except the email
	  unset( $fields['billing_country'] );
	  //unset( $fields['billing_first_name'] );
	  //unset( $fields['billing_last_name'] );
	  unset( $fields['billing_company'] );
	  unset( $fields['billing_address_1'] );
	  unset( $fields['billing_address_2'] );
	  unset( $fields['billing_city'] );
	  unset( $fields['billing_state'] );
	  unset( $fields['billing_postcode'] );
	  //unset( $fields['billing_phone'] );
		return $fields;
	}
	add_filter( 'woocommerce_billing_fields', 'patricks_billing_fields', 20 );

	function denda_billing_fields( $fields ) {
		global $woocommerce;

		// return the regular billing fields if we need shipping fields
		//echo $woocommerce->shipping->shipping_total;
		$chosen_methods = WC()->session->get( 'chosen_shipping_methods' );
 		$chosen_shipping = $chosen_methods[0];


	  // we don't need the billing fields so empty all of them except the email
	  unset( $fields['billing_country'] );
	  //unset( $fields['billing_first_name'] );
	  //unset( $fields['billing_last_name'] );
	  unset( $fields['billing_company'] );

	  //unset( $fields['billing_phone'] );
		return $fields;
	}
	add_filter( 'woocommerce_billing_fields', 'denda_billing_fields', 20 );

	/* FUNCTION AUTO LOGIN AFTER REGISTER */
	/*
	function auto_login_new_user( $user_id ) {

        wp_set_current_user($user_id);
        wp_set_auth_cookie($user_id);
        wp_safe_redirect( home_url().'/mi-cuenta/' );
        exit;
    }
    add_action( 'woocommerce_registration_redirect', 'auto_login_new_user' );
    */


    /*  FUNCTION SAVE LAST LOGIN */
    function function_last_login_user($login) {

        $user = get_user_by('login',$login);

        $count = get_user_meta($user->ID, '_count_login_user',1);

        if (get_user_meta($user->ID, '_last_login_user')) {

            update_user_meta($user->ID, '_count_login_user', $count+1);
            update_user_meta($user->ID, '_last_login_user', time() );
        }else{
            add_user_meta( $user->ID, '_last_login_user', time() );
            add_user_meta( $user->ID, '_count_login_user', 1 );
        }

    }
    add_action( 'wp_login', 'function_last_login_user');

add_action( 'init', 'pmg_rewrite_add_rewrites' );
function pmg_rewrite_add_rewrites()
{
    add_rewrite_rule(
        '^punto-pagos/fracaso/.*/?$', // p followed by a slash, a series of one or more digits and maybe another slash
        'index.php?page_id=13246',
        'top'
    );

    add_rewrite_rule(
        '^punto-pagos/exito/([^/]+)/?', // p followed by a slash, a series of one or more digits and maybe another slash
        'index.php?page_id=13242&puntopagos_token=$matches[1]',
        'top'
    );
    //flush_rewrite_rules(false);
}
add_filter( 'query_vars', 'puntopagostoken_query_vars' );
function puntopagostoken_query_vars( $query_vars )
{
    $query_vars[] = 'puntopagos_token';
    return $query_vars;
}


/**
 * Redirect users after add to cart.
 */
function my_custom_add_to_cart_redirect( $url ) {

    if(isset($_REQUEST['redirect_to'])){
        $url = wc_get_cart_url(); // URL to redirect to (1 is the page ID here)
    } else {
        $url = wp_get_referer();
    }

    return $url;
}
add_filter( 'woocommerce_add_to_cart_redirect', 'my_custom_add_to_cart_redirect' );


/*Add assets necesarios*/
function assets_utils() {

    wp_enqueue_script( 'script', get_template_directory_uri() . '/js/utils.js', array('jquery'));

	//inputmask
    wp_enqueue_script( 'mask', get_template_directory_uri() . '/js/inputmask.js', array('jquery') , 1.1, true);


	// jquery ui datepicker
	wp_enqueue_script('jquery-ui-core');

	wp_enqueue_script('jquery-ui-datepicker');

	wp_enqueue_style('jquery-ui-css', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
}

add_action( 'wp_enqueue_scripts', 'assets_utils' );


/**
 *  Función edita el formulario de mi cuenta. Agrega campos
 */
function my_woocommerce_edit_account_form() {

	$user_id = get_current_user_id();
	$user = get_userdata( $user_id );

	if ( !$user )
		return;

	woocommerce_form_field('account_phone_billing', array(
		'type'				=> 		'text',
		'class'				=>		array('woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-first'),
		'label_class'		=>		array('woocommerce-Input woocommerce-Input--text input-text'),
		'id'				=>		'account_phone_billing',
		'required'			=>		false,
		'label'				=>		'Teléfono',
		'custom_attributes' =>	array(' data-inputmask'=>'\'mask\': \'(+99 9) 9999-9999\'')
	),  get_user_meta( $user->ID, 'billing_phone', true));


	woocommerce_form_field('account_birthday', array(
		'type'				=> 		'text',
		'class'				=>		array('woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-last'),
		'label_class'		=>		array('woocommerce-Input woocommerce-Input--text input-text'),
		'id'				=>		'account_birthday',
		'required'			=>		false,
		'label'				=>		'Fecha de Nacimiento',
	), get_user_meta( $user->ID, 'account_birthday', true));

	woocommerce_form_field('account_sex', array(
		'type'				=> 		'radio',
		'class'				=>		array('woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide my-radio'),
		'input_class'		=>		array('fix_radio'),
		'label'				=>		'Sexo',
		'label_class'		=>		''	,
		'id'				=>		'account_sex',
		'required'			=>		false,
		'options'			=>		array('m' => 'Masculino', 'f' => 'Femenino')
	), get_user_meta( $user->ID, 'account_sex', true));


}

add_action( 'woocommerce_edit_account_form', 'my_woocommerce_edit_account_form' );

/* Function para guardar detalles de mi cuenta*/
function my_woocommerce_save_account_details( $user_id ) {


	$fono = '';
	$birthday = '';
	$sex = '';

	$validate = true;

	if (!$_POST['account_phone_billing']){
		//wc_add_notice( '<strong>Teléfono</strong> ' . __( 'is a required field.', 'woocommerce' ), 'error' );
		$validate = false;
	} else {
		$fono = wc_clean( $_POST['account_phone_billing'] );
	}



	if (!$_POST['account_birthday']){
		//wc_add_notice( '<strong>Fecha de Nacimiento</strong> ' . __( 'is a required field.', 'woocommerce' ), 'error' );
		$validate = false;
	} else {
		$birthday = wc_clean( $_POST['account_birthday'] );
	}



	if (!$_POST['account_sex']){
		//wc_add_notice( '<strong>Sexo</strong> ' . __( 'is a required field.', 'woocommerce' ), 'error' );
		$validate = false;
	} else {
		$sex = wc_clean( $_POST['account_sex'] );
	}

	if ($validate===true) {

		update_user_meta( $user_id, 'billing_phone', htmlentities(  $fono ) );
		update_user_meta( $user_id, 'account_birthday', htmlentities( $birthday ) );
		update_user_meta( $user_id, 'account_sex', htmlentities( $sex ) );
	}

}

add_action( 'woocommerce_save_account_details', 'my_woocommerce_save_account_details' );


//enqueues our locally supplied font awesome stylesheet
function enqueue_bancos(){
	wp_enqueue_style('bancos', get_stylesheet_directory_uri() . '/css/bancos.css');
}
add_action('wp_enqueue_scripts','enqueue_bancos');

/**
 * @snippet       Disable Variable Product Price Range
 * @how-to        Watch tutorial @ https://businessbloomer.com/?p=19055
 * @sourcecode    https://businessbloomer.com/disable-variable-product-price-range-woocommerce/
 * @author        Rodolfo Melogli
 * @compatible    WooCommerce 2.4.7
 */

add_filter( 'woocommerce_variable_sale_price_html', 'bbloomer_variation_price_format', 10, 2 );

add_filter( 'woocommerce_variable_price_html', 'bbloomer_variation_price_format', 10, 2 );

function bbloomer_variation_price_format( $price, $product ) {

// Main Price
$prices = array( $product->get_variation_price( 'min', true ), $product->get_variation_price( 'max', true ) );
$price = $prices[0] !== $prices[1] ? sprintf( __( 'desde %1$s', 'woocommerce' ), wc_price( $prices[0] ) ) : wc_price( $prices[0] );

// Sale Price
$prices = array( $product->get_variation_regular_price( 'min', true ), $product->get_variation_regular_price( 'max', true ) );
sort( $prices );
$saleprice = $prices[0] !== $prices[1] ? sprintf( __( 'desde %1$s', 'woocommerce' ), wc_price( $prices[0] ) ) : wc_price( $prices[0] );

if ( $price !== $saleprice ) {
$price = '<del>' . $saleprice . '</del> <ins>' . $price . '</ins>';
}
return $price;
}


add_filter( 'body_class', 'sp_body_class' );
function sp_body_class( $classes ) {

	if ( is_product_category() ){
		$product_cat_id = null;
		global $post;
		$term_obj = get_queried_object();

		if ($term_obj->parent == 0){
			$term_class_imagen = get_term_meta( $term_obj->term_id, 'class_image_header', true );
			$imagen = $term_class_imagen;

		}else{
			$term_class_imagen = get_term_meta( $term_obj->parent, 'class_image_header', true );
			$imagen = $term_class_imagen;

		}



		$classes[] = 'class_'.$imagen;
	}

	return $classes;

}


//Product Cat creation page
function text_domain_taxonomy_add_new_meta_field() {
	?>
	<div class="form-field">
		<label for="term_class_imagen"><?php _e('Clase imagen header', 'text_domain'); ?></label>
		<input type="text" name="term_class_imagen" id="term_class_imagen">
	</div>
	<?php
}

add_action('product_cat_add_form_fields', 'text_domain_taxonomy_add_new_meta_field', 10, 2);

//Product Cat Edit page
function text_domain_taxonomy_edit_meta_field($term) {




	// retrieve the existing value(s) for this meta field. This returns an array
	$term_class_imagen = get_term_meta( $term->term_id, 'class_image_header', true );

	?>
	<tr class="form-field">
		<th scope="row" valign="top"><label for="term_class_imagen"><?php _e('Clase imagen header', 'text_domain'); ?></label></th>
		<td>
			<input type="text" name="term_class_imagen" id="term_class_imagen" value="<?php echo esc_attr($term_class_imagen) ? esc_attr($term_class_imagen) : ''; ?>">
		</td>
	</tr>
	<?php
}

add_action('product_cat_edit_form_fields', 'text_domain_taxonomy_edit_meta_field', 10, 2);


// Save extra taxonomy fields callback function.
function save_taxonomy_custom_meta($term_id) {

	if ( ! isset( $_POST['term_class_imagen'] )) {
		return;
	}

	$class_image =  isset( $_POST['term_class_imagen'] )? $_POST['term_class_imagen']: '';

	update_term_meta($term_id, 'class_image_header', $class_image);
}

add_action('edited_product_cat', 'save_taxonomy_custom_meta', 10, 2);
add_action('create_product_cat', 'save_taxonomy_custom_meta', 10, 2);


/***************************    PODIO  UPDATER   **********************************************************/

/**
 * Create or update user podio
 * @param $user_id
 */
function create_or_update_user_podio($user_id)
{
	$user_info = get_userdata($user_id);

	//app_id && app_token
	//$app_id_users = 16659238;
	$app_id_users = 16820383;  //DENDA PROD
	//$app_token_users = '6d585a70f0f64cbb8ea08a06f4515f26';
	$app_token_users = '2ecd6888926046698e265f1a53a0fec8';

	//obtengo parametros ingresados en el plugin por la administración
	$client = get_option( 'podio_manager_client' );
	$secret = get_option( 'podio_manager_secret' );
	$email_notification = get_option( 'podio_manager_email_notification' );


	Podio::setup( $client, $secret);

	try {
		Podio::authenticate_with_app($app_id_users, $app_token_users);

		$attributes = array(
			'filters'=>array( 'email' => array($user_info->user_email))
		);

		$filter = PodioItem::filter($app_id_users, $attributes);

		if ($filter->filtered > 0) {
			foreach ($filter as $item) {
				// si existe el usuario en podio se actualiza
				updateUserPodio($item->id, $user_info);
			}
		} else {
			// si el usuario en podio es nuevo se crea
			addUserPodio($app_id_users, $user_info);
		}
	}
	catch (PodioBadRequestError $e) { // catch de errores y enviados a email ingresado en el plugin
		wp_mail($email_notification,'error', $e->body['error_description']);
	}

}
// hook para registro de usuarios
add_action('user_register', 'create_or_update_user_podio');
// hook para edición de usuarios
add_action('profile_update', 'create_or_update_user_podio');


/**
 * Funcion para agregar usuarios a Podio
 * @param $app_id_usuario
 * @param $user
 */
function addUserPodio($app_id_usuario, $user) {


	$registrado = 1;

	$collecionFields['username'] =  new PodioTextItemField( array( "external_id" => 'username', 'values' => $user->user_login ));

	$collecionFields['titulo'] =  new PodioTextItemField( array( "external_id" => 'titulo', 'values' => $user->nicename ));

	$collecionFields['apellido'] =  new PodioTextItemField( array( "external_id" => 'apellido', 'values' => '' ));

	$collecionFields['email'] = new PodioEmailItemField( array( "external_id" => 'email', 'values' => array( array( 'type' => 'home', 'value' => $user->user_email) )));

	$collecionFields['registrado'] = new PodioCategoryItemField( array( "external_id" => 'registrado', 'values' => $registrado ));

	$date = new DateTime($user->user_registered);
	$collecionFields['fecha-registro'] = new PodioDateItemField( array( "external_id" => 'fecha-registro', 'values' => array('start' => $date->format('Y-m-d'))));

	$collecionFields['ha-comprado'] = new PodioCategoryItemField( array( "external_id" => 'ha-comprado', 'values' => 0 ));

	$fields_final = new PodioItemFieldCollection($collecionFields);

	$item = new PodioItem(array(
		'app' => new PodioApp((int)$app_id_usuario), // Attach to app with app_id=123
		'fields' => $fields_final
	));

	$item->save();

}

/**
 * Función para actualizar usuarios en Podio
 * @param $item_id
 * @param $user
 */
function updateUserPodio($item_id, $user) {

	$podioItem = PodioItem::get($item_id);

	$first_name = isset($_POST['account_first_name'])?$_POST['account_first_name']:$_POST['first_name'];
	$last_name = isset($_POST['account_last_name'])?$_POST['account_last_name']:$_POST['last_name'];
	$email = isset($_POST['account_email'])?$_POST['account_email']:$_POST['email'];
	$registrado = 1;

	if (!isset($podioItem->fields['username'])) {
		$podioItem->fields['username'] = new PodioTextItemField();
	}
	$podioItem->fields['username']->values = $user->user_login;

	if ($first_name != '') {
		if (!isset($podioItem->fields['titulo'])) {
			$podioItem->fields['titulo'] = new PodioTextItemField();
		}
		$podioItem->fields['titulo']->values = $first_name;
	}

	if ($last_name != '') {
		if (!isset($podioItem->fields['apellido'])) {
			$podioItem->fields['apellido'] = new PodioTextItemField();
		}
		$podioItem->fields['apellido']->values = $last_name;
	}

	if ( $email != '' ) {
		if (!isset($podioItem->fields['email'])) {
			$podioItem->fields['email'] = new PodioEmailItemField();
		}
		$podioItem->fields['email']->values = array(array('type' => 'home', 'value' => $email));
	}

	if (!isset($podioItem->fields['registrado'])) {
		$podioItem->fields['registrado'] = new PodioCategoryItemField();
	}

	$podioItem->fields['registrado']->values = $registrado;

	$podioItem->save();

}

/**
 * Create or Update Podio Item
 * @param $post_ID
 * @param $post
 * @param $update
 */
function create_or_update_podio_item( $post_ID, $post, $update ) {

	if($post->post_type == 'shop_order') {

		//$app_id_orden = 16728971;
		$app_id_orden = 16820398; //DENDA PROD VENTAS
		//$app_token_orden = '7fb410a718194081bafa7ee8fafc845b';
		$app_token_orden = '4ee82cae806e4f2c81189ef995804e6d';


		$client = get_option( 'podio_manager_client' );
		$secret = get_option( 'podio_manager_secret' );
		$email_notification = get_option( 'podio_manager_email_notification' );

		Podio::setup( $client, $secret);



		try{
			Podio::authenticate_with_app($app_id_orden, $app_token_orden);


			$attributes = array(
				'filters'=>array( 'orden-id' => array( 'from' => $post_ID, 'to' => $post_ID ))
			);

			$filter = PodioItem::filter($app_id_orden, $attributes);


			if ($filter->filtered > 0) {

				foreach ($filter as $item) {

					//print_r($item);

					updateItemPodio($item->id, $post);


				}
			} else {

				addItemPodio($app_id_orden, $post);
			}

		}
		catch (PodioBadRequestError $e) {

			wp_mail($email_notification,'error', $e->body['error_description']);

		}


	}

}
add_action( 'save_post', 'create_or_update_podio_item', 10, 3 );

function createItemsOrder($order, $item_id) {
	$collecionOrden = [];

	$collecionOrden['orden-id'] = new PodioNumberItemField( array( 'external_id' => 'orden-id', 'values' => $order->id));

	$collecionOrden['orden'] = new PodioAppItemField( array( 'external_id' => 'orden', 'values' => array('item_id' => (int)$item_id)));

	// app_id de products production
	$app_id = 17473166;

	$items_order = $order->get_items();

	foreach ($items_order as $item) {
		$collecionOrden['product-id'] = new PodioNumberItemField( array( 'external_id' => 'product-id', 'values' => $item['product_id']));
		$collecionOrden['product-name'] = new PodioTextItemField( array( 'external_id' => 'product-name', 'values' => $item['name']));
		$collecionOrden['product-qty'] = new PodioNumberItemField( array( 'external_id' => 'product-qty', 'values' => $item['qty']));
		$collecionOrden['product-subtotal'] =  new PodioMoneyItemField( array( 'external_id' => 'product-subtotal', 'values' => array('value' =>  (float) $item['line_subtotal'], 'currency' => 'CLP')));
		$collecionOrden['product-total'] =  new PodioMoneyItemField( array( 'external_id' => 'product-total', 'values' => array('value' =>  (float) $item['line_total'], 'currency' => 'CLP')));

		$fields_order = new PodioItemFieldCollection($collecionOrden);

		$item = new PodioItem(array(
			'app' => new PodioApp((int)$app_id), // Attach to app with app_id=123
			'fields' => $fields_order
		));

		$item->save();

	}

	return true;
}

function updateItemPodio($item_id, $post) {

	$podioItem = PodioItem::get($item_id);

	$order = new WC_Order($post->ID);


	if (!isset($podioItem->fields['orden-id'])) {
		$podioItem->fields['orden-id'] = new PodioNumberItemField();
	}
	$podioItem->fields['orden-id']->values = $post->ID;


	if (!isset($podioItem->fields['sale-status-2'])) {
		$podioItem->fields['sale-status-2'] = new PodioCategoryItemField();
	}
	$opt=0;
	switch ($post->post_status) {
		case 'wc-completed':
			$opt = 1;
			createItemsOrder($order, $item_id);
			break;
		case 'wc-on-hold':
			$opt = 2;
			break;
		case 'wc-cancelled':
			$opt = 3;
			break;
		case 'wc-processing':
			$opt = 4;
			break;
		case 'wc-failed':
			$opt = 5;
			break;
		case 'wc-pending':
			$opt = 6;
			break;
	}


	$podioItem->fields['sale-status-2']->values = $opt;


	if (!isset($podioItem->fields['fecha-compra'])) {
		$podioItem->fields['fecha-compra'] = new PodioDateItemField();
	}
	$date = new DateTime($post->post_date);
	$podioItem->fields['fecha-compra']->values = array('start' => $date->format('Y-m-d'));


	if (!isset($podioItem->fields['medio-de-pago'])) {
		$podioItem->fields['medio-de-pago'] = new PodioCategoryItemField();
	}


	$method_payment = get_post_meta( $order->id, '_payment_method', true );

	$opt=0;

	if ($method_payment == 'bacs')
		$opt = 1;
	else if ($method_payment == 'greenti_puntopagos')
		$opt = 3;
	else
		$opt = 4;



	$podioItem->fields['medio-de-pago']->values = $opt;

	/*
	if ($order->get_total() > 0) {
		if (!isset($podioItem->fields['total'])) {
			$podioItem->fields['total'] = new PodioMoneyItemField();
		}
		$podioItem->fields['total']->values = array('value' => $order->get_total(), 'currency' => 'CLP');
	}
	*/


	/*
	$attributes = array('url' => get_site_url().'/wp-admin/post.php?post='.$post->ID.'&action=edit');
	$embed = PodioEmbed::create($attributes);

	$attribute['embed']['embed_id'] = $embed->embed_id;
	//$attribute['file']['file_id'] = $embed->files[0]->file_id;

	if (!isset($podioItem->fields['enlace'])) {
		$podioItem->fields['enlace'] = new PodioEmbedItemField();
	}
	$podioItem->fields['enlace']->values = $attribute;
	*/

	$podioItem->save();


	$usuario = $order->customer_user;

	addOrUpdateUserPodio($item_id, $usuario, $post);

}


function addItemPodio($app_id, $post) {

	$order = new WC_Order($post->ID);

	$collecionFields['orden-id'] = new PodioNumberItemField( array( 'external_id' => 'orden-id', 'values' => $order->id));

	$opt=0;
	switch ($post->post_status) {
		case 'wc-completed':
			$opt = 1;
			break;
		case 'wc-on-hold':
			$opt = 2;
			break;
		case 'wc-cancelled':
			$opt = 3;
			break;
		case 'wc-processing':
			$opt = 4;
			break;
		case 'wc-failed':
			$opt = 5;
			break;
		case 'wc-pending':
			$opt = 6;
			break;
	}
	$collecionFields['sale-status-2'] =  new PodioCategoryItemField( array( "external_id" => 'sale-status-2', 'values' => $opt ) );

	$date = new DateTime($order->order_date);
	$collecionFields['fecha-compra'] = new PodioDateItemField( array( "external_id" => 'fecha-compra', 'values' => array('start' => $date->format('Y-m-d'))));

	//$method_payment = get_post_meta( $order->id, '_payment_method', true );
	$method_payment = $_POST['payment_method'];

	if ($method_payment == 'bacs')
		$opt = 1;
	else if ($method_payment == 'greenti_puntopagos')
		$opt = 3;
	else
		$opt = 4;


	$collecionFields['medio-de-pago'] = new PodioCategoryItemField( array( "external_id" => 'medio-de-pago', 'values' => $opt ) );


	$collecionFields['total'] =  new PodioMoneyItemField( array( 'external_id' => 'total', 'values' => array('value' =>  (float) WC()->cart->total, 'currency' => 'CLP')));

	$attributes = array('url' => get_site_url().'/wp-admin/post.php?post='.$post->ID.'&action=edit');
	$embed = PodioEmbed::create($attributes);

	$attribute['embed']['embed_id'] = $embed->embed_id;
	//$attribute['file']['file_id'] = $embed->files[0]->file_id;
	$collecionFields['enlace'] =  new PodioEmbedItemField( array( 'external_id' => 'enlace', 'values' => $attribute));


	$fields_final = new PodioItemFieldCollection($collecionFields);

	$item = new PodioItem(array(
		'app' => new PodioApp((int)$app_id), // Attach to app with app_id=123
		'fields' => $fields_final
	));



	$item->save();




	$usuario = get_current_user_id();


	addOrUpdateUserPodio($item->item_id, $usuario, $post);


}


function addOrUpdateUserPodio($item_id_orden, $user_id, $post)
{

	$current_user = wp_get_current_user();

	$order = new WC_Order($post->ID);

	$_user = get_userdata($order->customer_user);


	if ($user_id == $current_user->ID && $user_id > 0 ) {
		$username = $current_user->user_login;
		$email = $_POST['billing_email']?$_POST['billing_email']:$current_user->user_email;
		$registrado = 1;
		$fecha_registro = $current_user->user_registered;
		$first_name = $_POST['billing_first_name']?$_POST['billing_first_name']:$current_user->first_name;
		$last_name = $_POST['billing_last_name']?$_POST['billing_last_name']:$current_user->last_name;
	} else {

		//Entra cuando actualizo un registro diferente a mi usuario y cuando realizo una compra sin registrarme

		$username = $_user->user_login;
		$email = isset($_POST['billing_email'])?$_POST['billing_email']:$order->billing_email;
		$registrado = 2;
		$fecha_registro = $order->order_date;
		$first_name = isset($_POST['billing_first_name'])?$_POST['billing_first_name']:$order->billing_first_name;
		$last_name = isset($_POST['billing_last_name'])?$_POST['billing_last_name']:$order->billing_last_name;
	}


	//$app_id_usuario = 16820383;
	$app_id_usuario = 16820383; //DENDA PROD
	//$app_token_usuario = '2ecd6888926046698e265f1a53a0fec8';
	$app_token_usuario = '2ecd6888926046698e265f1a53a0fec8';

	$email_notification = get_option( 'podio_manager_email_notification' );


	try {
		Podio::authenticate_with_app($app_id_usuario, $app_token_usuario);


		$attributes = array(
			'filters' => array('email' => array($email))
		);


		$filter = PodioItem::filter($app_id_usuario, $attributes);


		if ($filter->filtered > 0) {

			foreach ($filter as $item) {



				$podioItem = PodioItem::get($item->item_id);

				if (!isset($podioItem->fields['username'])) {
					$podioItem->fields['username'] = new PodioTextItemField();
				}
				$podioItem->fields['username']->values = $username;


				if (!isset($podioItem->fields['titulo'])) {
					$podioItem->fields['titulo'] = new PodioTextItemField();
				}
				$podioItem->fields['titulo']->values = $first_name;


				if (!isset($podioItem->fields['apellido'])) {
					$podioItem->fields['apellido'] = new PodioTextItemField();
				}
				$podioItem->fields['apellido']->values = $last_name;


				if (!isset($podioItem->fields['email'])) {
					$podioItem->fields['email'] = new PodioEmailItemField();
				}
				$podioItem->fields['email']->values = array(array('type' => 'home', 'value' => $email));


				$relationApps = array();
				if (!isset($podioItem->fields['relacion'])) {
					$podioItem->fields['relacion'] = new PodioAppItemField();
					$relationApps = array('item_id' => $item_id_orden);
				} else {

					foreach ($podioItem->fields['relacion']->values as $pitem) {
						array_push($relationApps, array('item_id' =>$pitem->item_id));
					}
					array_push($relationApps, array('item_id' => $item_id_orden));
				}

				//$podioItem->fields[$field->external_id]->values = array(array('item_id'  => 481259884),array('item_id'  => 481254797));
				$podioItem->fields['relacion']->values = $relationApps;


				if (!isset($podioItem->fields['registrado'])) {
					$podioItem->fields['registrado'] = new PodioCategoryItemField();
				}

				$podioItem->fields['registrado']->values = $registrado;

				if (!isset($podioItem->fields['fecha-registro'])) {
					$podioItem->fields['fecha-registro'] = new PodioDateItemField();
					$date = new DateTime($fecha_registro);
					$podioItem->fields['fecha-registro']->values = array('start' => $date->format('Y-m-d'));
				}

				if (!isset($podioItem->fields['ha-comprado'])) {
					$podioItem->fields['ha-comprado'] = new PodioCategoryItemField();
				}

				$podioItem->fields['ha-comprado']->values = 1;


				$podioItem->save();

			}
		} else {

			$collecionFields['username'] =  new PodioTextItemField( array( "external_id" => 'username', 'values' => '' ));

			$collecionFields['titulo'] =  new PodioTextItemField( array( "external_id" => 'titulo', 'values' => $first_name ));

			$collecionFields['apellido'] =  new PodioTextItemField( array( "external_id" => 'apellido', 'values' => $last_name ));

			$collecionFields['email'] = new PodioEmailItemField( array( "external_id" => 'email', 'values' => array( array( 'type' => 'home', 'value' => $email) )));

			$relationApps =  array();
			array_push($relationApps, array('item_id' => $item_id_orden));
			$collecionFields['relacion'] = new PodioAppItemField( array( 'external_id' => 'relacion', 'values' => $relationApps));

			$collecionFields['registrado'] = new PodioCategoryItemField( array( "external_id" => 'registrado', 'values' => $registrado ));

			$date = new DateTime($fecha_registro);
			$collecionFields['fecha-registro'] = new PodioDateItemField( array( "external_id" => 'fecha-registro', 'values' => array('start' => $date->format('Y-m-d'))));

			$collecionFields['ha-comprado'] = new PodioCategoryItemField( array( "external_id" => 'ha-comprado', 'values' => 1 ));

			$fields_final = new PodioItemFieldCollection($collecionFields);

			$item = new PodioItem(array(
				'app' => new PodioApp((int)$app_id_usuario), // Attach to app with app_id=123
				'fields' => $fields_final
			));


			$item->save();

		}



	} catch (PodioBadRequestError $e) {

		wp_mail($email_notification, 'error', $e->body['error_description']);

	}

	return true;
}

/***************************  FIN  PODIO  UPDATER   **********************************************************/


/**
 * Register new endpoint to use inside My Account page.
 *
 * @see https://developer.wordpress.org/reference/functions/add_rewrite_endpoint/
 */
function my_custom_endpoints() {
	add_rewrite_endpoint( 'favoritos', EP_ROOT | EP_PAGES );
}

add_action( 'init', 'my_custom_endpoints' );

/**
 * Add new query var.
 *
 * @param array $vars
 * @return array
 */
function my_custom_query_vars( $vars ) {
	$vars[] = 'favoritos';

	return $vars;
}

add_filter( 'query_vars', 'my_custom_query_vars', 0 );


/**
 * Flush rewrite rules on theme activation.
 */
function my_custom_flush_rewrite_rules() {
	add_rewrite_endpoint( 'favoritos', EP_ROOT | EP_PAGES );
	flush_rewrite_rules();
}

add_action( 'after_switch_theme', 'my_custom_flush_rewrite_rules' );


/**
 * Insert the new endpoint and remove download item into the My Account menu.
 *
 * @param array $items
 * @return array
 */
function my_custom_my_account_menu_items( $items ) {



	unset( $items['downloads']);

	$address = $items['edit-address'];
	unset($items['edit-address']);

	$account = $items['edit-account'];
	unset($items['edit-account']);

	$logout = $items['customer-logout'];
	unset( $items['customer-logout'] );

	//yith_wcwl_is_wishlist_page();
	//die('test');


	$items['favoritos'] = __( 'Mis favoritos', 'woocommerce' );



	$items['edit-address'] = $address;
	$items['edit-account'] = $account;
	$items['customer-logout'] = $logout;




	return $items;
}

add_filter( 'woocommerce_account_menu_items', 'my_custom_my_account_menu_items' );

/**
 * Endpoint HTML content.
 */
function my_custom_endpoint_content() {
	global $yith_wcwl;

	if ( wp_redirect( $yith_wcwl->get_wishlist_url() ) ) {
		exit;
	}
}

add_action( 'woocommerce_account_favoritos_endpoint', 'my_custom_endpoint_content' );

add_filter( 'woocommerce_product_tabs', 'wp_woo_rename_reviews_tab', 98);
function wp_woo_rename_reviews_tab($tabs) {

	unset($tabs['reviews']);

	return $tabs;
}

function woocommerce_template_product_reviews() {
	woocommerce_get_template( 'single-product-reviews.php' );
}
add_action( 'woocommerce_after_single_product_summary', 'comments_template', 10 );


/*Add assets necesarios*/
function script_scrolls() {

	//sticky
	wp_enqueue_script( 'sticky', get_template_directory_uri() . '/js/stickyfill.js', '' , false);

}

add_action( 'wp_enqueue_scripts', 'script_scrolls' );

// define the woocommerce_email_customer_details_fields callback
function filter_woocommerce_email_customer_details_fields( $fields, $sent_to_admin, $order ) {

	if ($order->billing_state) {

		global $woocommerce;
		$countries_obj   = new WC_Countries();
		$countries   = $countries_obj->__get('countries');
		$default_country = $countries_obj->get_base_country();
		$default_county_states = $countries_obj->get_states( $default_country );

		foreach ($default_county_states as $key => $default_county_state) {
			if ($key == $order->billing_state) {
				$state = $default_county_state;
			}
		}



		$fields['billing_state'] = array(
			'label' => __( 'Comuna', 'woocommerce' ),
			'value' => wptexturize( $state )
		);

	}

	return $fields;
};


add_filter( 'woocommerce_email_customer_details_fields', 'filter_woocommerce_email_customer_details_fields', 10, 3 );




function woo_custom_order_formatted_billing_address( $address, $wc_order ) {

	global $woocommerce;
	$countries_obj   = new WC_Countries();
	$countries   = $countries_obj->__get('countries');
	$default_country = $countries_obj->get_base_country();
	$default_county_states = $countries_obj->get_states( $default_country );

	foreach ($default_county_states as $key => $default_county_state) {
		if ($key == $wc_order->billing_state) {
			$state = $default_county_state;
		}
	}


	// make the changes to $address array here
	// use for example, $wc_order->billing_first_name, instead of $this->billing_first_name
	$address = array(
		'postcode'      => $wc_order->billing_postcode,
		'last_name'     => $wc_order->billing_last_name,
		'first_name'    => $wc_order->billing_first_name,
		// custom field below, note the _ before name _billing_last_name_2
		'last_name_2'       => get_post_meta( $wc_order->id, '_billing_last_name_2', true ),
		'address_1'     => $wc_order->billing_address_1,
		'address_2'     => $wc_order->billing_address_2,
		'city'          => $wc_order->billing_city,
		'state'         => $state,

		'country'       => $wc_order->billing_country
	);
	return $address;
}

// add the filter
add_filter( 'woocommerce_order_formatted_billing_address' , 'woo_custom_order_formatted_billing_address', 10, 2 );


function woo_custom_order_formatted_shipping_address( $address, $wc_order ) {

	global $woocommerce;
	$countries_obj   = new WC_Countries();
	$countries   = $countries_obj->__get('countries');
	$default_country = $countries_obj->get_base_country();
	$default_county_states = $countries_obj->get_states( $default_country );

	foreach ($default_county_states as $key => $default_county_state) {
		if ($key == $wc_order->shipping_state) {
			$state = $default_county_state;
		}
	}


	// make the changes to $address array here
	// use for example, $wc_order->billing_first_name, instead of $this->billing_first_name
	$address = array(
		'postcode'      => $wc_order->shipping_postcode,
		'last_name'     => $wc_order->shipping_last_name,
		'first_name'    => $wc_order->shipping_first_name,
		// custom field below, note the _ before name _billing_last_name_2
		'last_name_2'       => get_post_meta( $wc_order->id, '_billing_last_name_2', true ),
		'address_1'     => $wc_order->shipping_address_1,
		'address_2'     => $wc_order->shipping_address_2,
		'city'          => $wc_order->shipping_city,
		'state'         => $state,

		'country'       => $wc_order->shipping_country
	);
	return $address;
}

// add the filter
add_filter( 'woocommerce_order_formatted_shipping_address' , 'woo_custom_order_formatted_shipping_address', 10, 2 );


remove_filter('the_content', 'wpautop');

// add_filter( ‘xmlrpc_methods’, function( $methods ) {
// 	unset( $methods['pingback.ping'] );
// 	return $methods;
// } );