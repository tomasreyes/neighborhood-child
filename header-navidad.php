<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>Denda | Especial Navidad</title>
  <!-- Sets initial viewport load and disables zooming  -->
  <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
  <!-- SmartAddon.com Verification -->
<link rel="icon" href="https://denda.cl/wp-content/uploads/2016/06/cropped-dendafav-2-32x32.png" sizes="32x32" />
<link rel="icon" href="https://denda.cl/wp-content/uploads/2016/06/cropped-dendafav-2-192x192.png" sizes="192x192" />
<link rel="apple-touch-icon-precomposed" href="https://denda.cl/wp-content/uploads/2016/06/cropped-dendafav-2-180x180.png" />
<meta name="msapplication-TileImage" content="https://denda.cl/wp-content/uploads/2016/06/cropped-dendafav-2-270x270.png" /> 


 <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css?ver=4.5.4">
  <link rel="stylesheet" href="https://denda.cl/wp-content/themes/neighborhood-child/css/bancos.css?ver=4.5.4" > 
 <!-- site css -->
  <link rel="stylesheet" href="https://s3-sa-east-1.amazonaws.com/denda.cl/especial-navidad/css/site.min.css">
  <!-- navidad css -->
  <link rel="stylesheet" href="https://s3-sa-east-1.amazonaws.com/denda.cl/especial-navidad/css/navidad.css">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800,700,400italic,600italic,700italic,800italic,300italic" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Dancing+Script" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Noto+Sans" rel="stylesheet">
<link rel='stylesheet' id='fontawesome-css-css'  href='https://denda.cl/wp-content/themes/neighborhood/css/font-awesome.min.css' type='text/css' media='all' />  
<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
  <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
  <script type="text/javascript" src="https://s3-sa-east-1.amazonaws.com/denda.cl/especial-navidad/js/site.min.js"></script>
<script type='text/javascript' src='https://denda.cl/wp-content/themes/neighborhood/js/stickyfill.js?ver=4.5.4'></script>
  
  <meta name="description" content="Dale un toque verde a tu Navidad regalando a los que más quieres. Sorprendentes productos,amigables con el medio ambiente y que mejorarán tu calidad de vida"/>
  <meta name="robots" content="noodp"/>
  <link rel="canonical" href="https://denda.cl/especial-navidad/" />
  <meta property="og:locale" content="es_ES" />
  <meta property="og:type" content="article" />
  <meta property="og:title" content="Especial Navidad | Denda" />
  <meta property="og:description" content="Dale un toque verde a tu Navidad regalando a los que más quieres. Sorprendentes productos,amigables con el medio ambiente y que mejorarán tu calidad de vida" />
  <meta property="og:url" content="https://denda.cl/especial-navidad/" />
  <meta property="og:site_name" content="Denda" />
  <meta property="article:publisher" content="https://www.facebook.com/Denda-Market-1548017622169106" />
  <meta property="og:image" content="https://denda.cl/wp-content/uploads/2016/11/especial-navidad-denda.png" />
  <meta name="twitter:card" content="summary_large_image" />
  <meta name="twitter:description" content="Dale un toque verde a tu Navidad regalando a los que más quieres. Sorprendentes productos,amigables con el medio ambiente y que mejorarán tu calidad de vida" />
  <meta name="twitter:title" content="Especial Navidad | Denda" />
  <meta name="twitter:site" content="@dendamarket" />
  <meta name="twitter:image" content="https://denda.cl/wp-content/uploads/2016/11/especial-navidad-denda.png" />
  <meta name="twitter:creator" content="@dendamarket" />
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-78544836-1', 'auto');
ga('send', 'pageview');
</script>
  <?php //wp_head(); ?>
</head>

<body>

  <!--Docs Header -->
  <div class="docs-header">
    <!--nav-->
    <nav class="navbar navbar-default navbar-custom" role="navigation">
      <div class="container">
        <div class="navbar-header" style="width:600px;">
          <div>
            <div id="logo" class="col-md-5">
              <a href="http://denda.cl"><img src="https://denda.cl/wp-content/uploads/2016/07/dendalogo_bco.svg" height="40" style="zoom:70%;"> </a> <span style="color:white; font-size: 2em;"></span>
            </div>
            <div id="slogan" class="col-md-6">
              <p class="quarrel-font" style="font-size: 2em; font-family: 'Dancing Script', cursive; color:white;"> Especial de Navidad</p>
            </div>
          </div>
        </div>
        <div class="collapse navbar-collapse">
	<ul id="social-header" class="social-icons standard light">
		<li class="facebook"><a href="https://www.facebook.com/Denda-Market-1548017622169106" target="_blank">Facebook</a></li>
		<li class="twitter"><a href="http://www.twitter.com/dendamarket" target="_blank">Twitter</a></li>
		<li class="instagram"><a href="http://instagram.com/dendamarket" target="_blank">Instagram</a></li>
	</ul>
          <!-- ul class="nav navbar-nav navbar-right">
          </ul-->
        </div>
      </div>
    </nav><!-- end nav-->
    <!-- Banner-->
    <div class="topic">
      <div class="container">
        <div class="col-md-12" class="navidad-header">
          <h3 class="quarrel-font" style="text-align: center;">Especial de Navidad</h3>
        </div>
      </div>
    </div><!-- End Banner-->
  </div><!--End Docs Header -->
  <!-- Gifts-->
  <div class="color-wrap clearfix">
    <div class="color-picker">
      <i class="icon13 top-left"></i> <i class="icon22 top-right"></i> <i class="icon27 bottom-right"></i>
      <div class="color-item" style="background-color:#D84533;">
        <span class="label-for">Para:</span>
         
        <span class="label-subject">Amigo</span>
         
        <span class="label-subject"><b>secreto</b></span>
         
      </div>
      <a href="https://denda.cl/etiqueta-producto/secreto/" class="btn btn-success ">VER REGALOS</a>
    </div>
    <div class="color-picker">
      <i class="icon13 top-left"></i> <i class="icon22 top-right"></i> <i class="icon20 bottom-right"></i>
      <div class="color-item" style="background-color:#37865D;">
        <span class="label-for">Para:</span>
         
        <span class="label-subject">Los más</span>
         
        <span class="label-subject"><b>pequeños</b></span>
         
      </div>
      <a href="https://denda.cl/etiqueta-producto/pequenos/" class="btn btn-success ">VER REGALOS</a>
    </div>
    <div class="color-picker">
      <i class="icon13 top-left"></i> <i class="icon22 top-right"></i> <i class="icon15 bottom-right"></i>
      <div class="color-item" style="background-color:#196F8B;">
        <span class="label-for">Para:</span>
         
        <span class="label-subject">Los súper</span>
         
        <span class="label-subject"><b>fitness</b></span>
         
      </div>
      <a href="https://denda.cl/etiqueta-producto/fitness/" class="btn btn-success ">VER REGALOS</a>
    </div>
    <div class="color-picker">
      <i class="icon13 top-left"></i> <i class="icon22 top-right"></i> <i class="icon4 bottom-right"></i>
      <div class="color-item" style="background-color:#E5CA5D;">
        <span class="label-for">Para:</span>
         
        <span class="label-subject">Amantes de</span>
         
        <span class="label-subject">la <b>cocina</b></span>
         
      </div>
      <a href="https://denda.cl/etiqueta-producto/cocina/" class="btn btn-success ">VER REGALOS</a>
    </div>
    <div class="color-picker">
      <i class="icon13 top-left"></i> <i class="icon22 top-right"></i> <i class="icon12 bottom-right"></i>
      <div class="color-item" style="background-color:#4B528B;">
        <span class="label-for">Para:</span>
         
        <span class="label-subject">Mente</span>
         
        <span class="label-subject"><b>smart</b></span>
         
      </div>
      <a href="https://denda.cl/etiqueta-producto/smart/" class="btn btn-success ">VER REGALOS</a>
    </div>
    <div class="color-picker">
      <i class="icon13 top-left"></i> <i class="icon22 top-right"></i> <i class="icon16 bottom-right"></i>
      <div class="color-item" style="background-color:#8C9847;">
        <span class="label-for">Para:</span>
         
        <span class="label-subject">Mejorar tu</span>
         
        <span class="label-subject"><b>hogar</b></span>
         
      </div>
      <a href="https://denda.cl/etiqueta-producto/hogar/" class="btn btn-success ">VER REGALOS</a>
    </div>

    <div class="color-picker">
      <i class="icon13 top-left"></i> <i class="icon22 top-right"></i> <i class="icon9 bottom-right"></i>
      <div class="color-item" style="background-color:#E16764;">
        <span class="label-for">Para:</span>
         
        <span class="label-subject">Cuidado</span>
         
        <span class="label-subject">y <b>belleza</b></span>
         
      </div>
      <a href="https://denda.cl/etiqueta-producto/belleza/" class="btn btn-success ">VER REGALOS</a>
    </div>
    <div class="color-picker">
      <i class="icon13 top-left"></i> <i class="icon22 top-right"></i> <i class="icon19 bottom-right"></i>
      <div class="color-item" style="background-color:#78B997;">
        <span class="label-for">Para:</span>
         
        <span class="label-subject">Los más</span>
         
        <span class="label-subject"><b>green</b></span>
         
      </div>
      <a href="https://denda.cl/etiqueta-producto/green/" class="btn btn-success ">VER REGALOS</a>
    </div>
    <div class="color-picker">
      <i class="icon13 top-left"></i> <i class="icon22 top-right"></i> <i class="icon20 bottom-right"></i>
      <div class="color-item" style="background-color: #999090;">
        <span class="label-for">Para:</span>
         
        <span class="label-subject">El <b>Hombre</b></span>
         
        <span class="label-subject">de la casa</span>
      </div>
      <a href="https://denda.cl/etiqueta-producto/hombre/" class="btn btn-success ">VER REGALOS</a>
    </div>
    <div class="color-picker">
      <i class="icon13 top-left"></i> <i class="icon22 top-right"></i> <i class="icon18 bottom-right"></i>
      <div class="color-item" style="background-color:#663D89;">
        <span class="label-for">Para:</span>
        <span class="label-subject">Los outdoor</span>
        <span class="label-subject"><b>extremos</b></span>
      </div>
      <a href="https://denda.cl/etiqueta-producto/outdoor/" class="btn btn-success ">VER REGALOS</a>
    </div>
    <div class="color-picker">
      <i class="icon13 top-left"></i> <i class="icon22 top-right"></i> <i class="icon11 bottom-right"></i>
      <div class="color-item" style="background-color:#D5B240;">
        <span class="label-for">Para:</span>
        <span class="label-subject">Los <b>jardineros</b></span>
        <span class="label-subject">de balcón</span>
      </div>
      <a href="https://denda.cl/etiqueta-producto/jardin/" class="btn btn-success ">VER REGALOS</a>
    </div>
    <div class="color-picker">
      <i class="icon13 top-left"></i> <i class="icon22 top-right"></i> <i class="icon25 bottom-right"></i>
      <div class="color-item" style="background-color:#B94552;">
        <span class="label-for">Para:</span>
        <span class="label-subject">La <b>mascota</b></span>
        <span class="label-subject">regalona</span>
      </div>
      <a href="https://denda.cl/etiqueta-producto/mascota/" class="btn btn-success ">VER REGALOS</a>
    </div>
  </div>
  <!-- end Gifts-->
  <!-- visit-us -->
  <div class=" visit-us">
    <div class="container">
      <div class="row">
        <div class="col-md-12" style="text-align: center;">
          <a href="https://denda.cl/tienda" class="btn btn-danger btn-lg btn-store"> IR A LA TIENDA</a>
        </div>
      </div>
    </div>
  </div>
  <!-- end visit-us -->
  </div>
  </div>
