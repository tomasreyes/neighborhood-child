<?php
/**
 * Template Name: Landing Paises
 *
 * @package WordPress
 */
?>


<?php
global $wpdb;

  $subscribed = false;


    if ( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] ) && $_POST['action'] == 'subscribe-email-denda' ) {

        /* Update email*/
        if ( !empty( $_POST['email'] ) ){
            if (!is_email(esc_attr( $_POST['email'] )))
                $error[] = __('El email no es válido.  Por favor ingresar un email válido.', 'profile');
            else{
              $email = $_POST['email'];
              $referer = $_POST['referer'];
              $fecha = date('Y-m-d H:i:s');
              //$wpdb->query("INSERT INTO mb_email_subscription (email, fecha) VALUES ('cristian@socialweb.cl', '2016-01-01')");


              $wpdb->insert( $wpdb->prefix . 'email_subscription',
              array(
                'email'     => trim($email),
                'referer' => trim($referer),
                'fecha' => trim($fecha)
              ),
              array(
                '%s',
                '%s',
                '%s')
              );

              $subscribed = true;
              //wp_redirect( get_permalink() );
              //exit;

            }
        }
    }
  ?>

 <!DOCTYPE html>
 <html lang="en">
   <head>
     <meta charset="utf-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
     <meta name="description" content="">
     <meta name="author" content="">
     <link rel="icon" href="../../favicon.ico">

     <title>Denda market</title>

     <link href='https://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic' rel='stylesheet' type='text/css'>
     <link href="https://s3-sa-east-1.amazonaws.com/denda.cl/landing-paises/css/bootstrap.min.css" rel="stylesheet">
     <link href="https://s3-sa-east-1.amazonaws.com/denda.cl/landing-paises/css/animate.css" rel="stylesheet">
     <link href="https://s3-sa-east-1.amazonaws.com/denda.cl/landing-paises/css/cover.css" rel="stylesheet">

     <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
     <!--[if lt IE 9]>
       <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
       <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
     <![endif]-->
   </head>

   <body>

     <div class="site-wrapper">

       <div class="site-wrapper-inner">

         <div class="cover-container">

           <div class="inner cover">
             <?php if($subscribed):?>
                 <p>Gracias , te avisaremos cuando estemos en tu país</p>
             <?php else: ?>

             <h1 class="cover-heading animated">¡Hola!</h1>
             <div class="tada animated">
               <p class="lead">En <strong>Denda</strong> encontrarás productos que <span id="js-rotating">mejoran tu vida, te hacen feliz, son inteligentes, quieren a nuestro planeta, son innovadores, te cuidan, hacen bien, son sustentables, sacan sonrisas, cuidan el medioambiente, son amigables</span></p>
               <p>Si quieres conocer nuestra tienda online,<br /> deja tu correo y  serás el primero en enterarte.</p>

               <form id="bedenda" class="form-inline" method="post" action="<?php the_permalink(); ?>" enctype="multipart/form-data" >
                   <div class="form-group">
                     <div class="input-group">
                       <input name="email" type="email" class="form-control" id="email"  placeholder="Ingresa tu correo electrónico" autofocus />
                       <span class="input-group-btn">
                         <button type="submit" class="btn btn-primary">Avísame</button>
                       </span>
                       <input name="action" type="hidden" id="action" value="subscribe-email-denda" />
                       <input name="referer" type="hidden" id="referer" value="<?php echo $_SERVER['HTTP_REFERER']?>" />
                     </div>
                   </div>
               </form>



               <?php endif; ?>
               </div>
           </div>

           <div class="mastfoot">
             <div class="container">
               <p>©2016 denda · con amor para el mundo.</p>
             </div>
           </div>

         </div>

       </div>

     </div>

     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
     <script src="https://s3-sa-east-1.amazonaws.com/denda.cl/landing-paises/js/bootstrap.min.js"></script>
     <script src="https://s3-sa-east-1.amazonaws.com/denda.cl/landing-paises/js/morphext.min.js"></script>
     <script>
       $(document).ready(function(){
           $('.tada').hide();
           setTimeout(function(){
              $(".cover-heading").addClass("fadeOut");
           }, 3000);
           setTimeout(function(){
              $('.cover-heading').hide();
              $(".tada").show();
              $(".tada").addClass("fadeIn");
              $( "#inputMail" ).focus();
           }, 4000);
           $("#js-rotating").Morphext({
             animation: "fadeIn",
             separator: ",",
             speed: 3000
           });
       });
     </script>
   </body>
 </html>
