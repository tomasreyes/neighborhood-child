<?php
/**
 * Template Name: Exito Page Punto pagos
 *
 * @package WordPress
 */
?>

<?php
ob_start();
?>
<?php get_header(); ?>
	
<?php

global $wp_query;



function get_object_by_token($token){
	global $woocommerce;



	$PUNTOPAGOS_TOKEN=$token;


	$query= new WP_Query(array('post_type'=>'shop_order', 'post_status' => 'wc-pending','meta_key'=>'PUNTOPAGOS_TOKEN','meta_value'=>$PUNTOPAGOS_TOKEN));

	//print_r($query);
	if ($query->have_posts()){
		while ($query->have_posts()){



			$query->the_post();
			$order_id = get_the_ID() ;

			$wp_order_key = get_post_meta(get_the_ID(),'_order_key');



			$order = new WC_Order($order_id);
		}



		return array('order_id' => $order->id, 'wp_order' => $wp_order_key[0]);
	}else{
		return 'error';
	}
}


function update_status_order($order, $token) {

	$order_wc = new WC_Order($order['order_id']);


	$url = home_url('/');

	$status_real = puntopagos_traer($order_wc->id, $token);




	if($status_real['EXITO']){
		update_post_meta($order_wc->id, "PUNTOPAGOS_EXITO", $status_real['RESPONSE']);
		$order_wc->payment_complete();
		//return $order;

		$url_2 = 'finalizar-compra/order-received/'.$order_wc->id.'/?key='.$order['wp_order'].'&utm_nooverride=1';

		$url = $url.$url_2;




		if ($order_wc->post_status != 'failed') {


			return $url;
		}
	}
	else{
		update_post_meta($order->id, "PUNTOPAGOS_ERROR2", $status_real['RESPONSE']);

		$order_wc->update_status('failed');
		//return $order;


		$url_2 = 'punto-pagos/fracaso';

		$url = $url.$url_2;

		return $url;
	}
}


function puntopagos_traer($order_id, $puntopagos_token){



	$options_plugin = get_option('woocommerce_greenti_puntopagos_settings');



	$PUNTOPAGOS_URL = $options_plugin['PUNTOPAGOS_URL'];
	$PUNTOPAGOS_SECRET = $options_plugin['PUNTOPAGOS_SECRET'];
	$PUNTOPAGOS_KEY = $options_plugin['PUNTOPAGOS_KEY'];

	$funcion = "transaccion/traer";
	$order = new WC_Order($order_id);
	$trx_id = $order->id;
	$monto = $order->order_total;
	$monto_str = number_format($monto, 2, '.', '');

	$http_request = $data_paso6 = '{"trx_id":"'.$trx_id.'","monto":'.$monto_str.'}';
	$http_request .= "<br/><br/>";

	$fecha = gmdate("D, d M Y H:i:s", time())." GMT";
	$mensaje = $funcion."\n".$puntopagos_token."\n".$trx_id."\n".$monto_str."\n".$fecha;
	$signature = base64_encode(hash_hmac('sha1', $mensaje, $PUNTOPAGOS_SECRET, true));
	$firma = "PP ".$PUNTOPAGOS_KEY.":".$signature;

	$header = array();
	$http_request .= $header[] = "Accept: application/json;";
	$http_request .= $header[] = "Accept-Charset: utf-8;";
	$http_request .= $header[] = "Accept-Language: en-us,en;q=0.5";
	$http_request .= $header[] = "Content-type: application/json";
	$http_request .= $header[] = "Fecha: ".$fecha;
	$http_request .= $header[] = "Autorizacion: ".$firma;



	$url_pp = $PUNTOPAGOS_URL."/transaccion/".$puntopagos_token;

	$curl = curl_init($url_pp);
	curl_setopt($curl, CURL_VERSION_SSL,"SSL_VERSION_SSLv3"); //optativo
	curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
	curl_setopt($curl, CURL_HTTP_VERSION_1_1, 1);
	//curl_setopt($curl, CURLOPT_POST,1);
	//curl_setopt($curl, CURLOPT_POSTFIELDS, $data_paso6);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	$http_response = curl_exec ($curl);
	//var_dump($http_response);
	$response = json_decode($http_response);
	curl_close($curl);



	if($response->respuesta == "00"){
		return array('EXITO'=>TRUE,'RESPONSE'=>print_r($response,true));
	}else{
		return array('EXITO'=>FALSE,'RESPONSE'=>print_r($response,true));
	}
}


$token = $wp_query->query_vars['puntopagos_token'];
$order = get_object_by_token($token);




if($order != 'error'){

	$redirect = update_status_order($order, $token);
	wp_redirect($redirect);
	exit();
} else {
	echo 'El pedido ya no está disponible para pago';
}



//mail('hector.varas@engiefactory.com','headers',$header_read);

?>


<?php get_footer(); ?>
<?php
ob_flush();
?>
