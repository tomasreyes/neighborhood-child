<?php
/**
 * Customer Reset Password email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-reset-password.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<p>Acabamos de recibir una <strong>solicitud para recuperar la contraseña de tu cuenta en Denda</strong>, pero queremos estar seguro de que fuiste tú.</p>

<p>Si no lo hiciste, este mensaje se autodestruirá en 5 segundos. Por el contrario, <strong>si ya no la recuerdas y necesitas otra, haz clic en el siguiente enlace</strong>: <a class="link" href="<?php echo esc_url( add_query_arg( array( 'key' => $reset_key, 'login' => rawurlencode( $user_login ) ), wc_get_endpoint_url( 'lost-password', '', wc_get_page_permalink( 'myaccount' ) ) ) ); ?>">
			<?php _e( recurperar mi contraseña, 'woocommerce' ); ?></a>.</p>

<p>Anótala bien o déjala guardada en tu computador. Además, recuerda que también <strong>puedes ingresar con tu cuenta de Facebook o de Google</strong>.</p>

<p>Ahora accede a tu cuenta y continúa vitrineando.</p>

<p>¡Te esperamos!</p>

<p>El equipo Denda</p>

<?php do_action( 'woocommerce_email_footer', $email ); ?>