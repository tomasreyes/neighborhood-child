<?php
/**
 * Customer new account email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-new-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<p><?php printf( __( "Gracias por crear tu cuenta en Denda. Tu nombre de usuario será <strong>%s</strong>.", 'woocommerce' ), esc_html($user_login)); ?></p>

<p>Por tu seguridad, no revelaremos tu contraseña en este correo electrónico pero podrás cambiarla directamente en tu cuenta.</p>

<p>Ya puedes vitrinear y comprar todo lo que te guste.</p>

<p>Aquí encontrarás los productos y servicios sustentables que mejorarán tu calidad vida ;).<p>

<p>Además, como bienvenida, al final de este correo encontrarás un <strong>regalo especial en tu primera compra.</strong></p>

<p>¡Te esperamos en <a href="https://denda.cl" class="azul-denda">denda.cl</a>!</p>

<p><strong>El equipo Denda</strong></p>

<p>
<img src="https://denda.cl/wp-content/uploads/2016/10/promo10.png" width="100%"/>
</p>

<?php do_action( 'woocommerce_email_footer', $email ); ?>

