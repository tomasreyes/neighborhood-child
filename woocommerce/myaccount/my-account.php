<?php
/**
 * My Account page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

wc_print_notices();


?>
<?php
$user = get_user_by('id', get_current_user_id());
$mostrar = false;

if ( !get_user_meta($user->ID, '_count_login_user', 1) )
	$mostrar = true;

if ( !get_user_meta($user->ID, 'billing_first_name',1) && !get_user_meta($user->ID, 'first_name', 1))
	$mostrar = true;


if ($mostrar):
?>
<div class="woocommerce-ticket"><i class="fa fa-fw fa-check" aria-hidden="true"></i><span>¡Ya eres parte de Denda! <strong>Completa tus datos personales</strong> o conoce los productos que tenemos para ti</span><a href="/tienda" type="button" class="btn btn-tienda pull-right">Ver tienda</a></div>

<?php
endif;
?>


<?php
/**
 * My Account navigation.
 * @since 2.6.0
 */
do_action( 'woocommerce_account_navigation' ); ?>

<div class="woocommerce-MyAccount-content">

	<?php
		/**
		 * My Account content.
		 * @since 2.6.0
		 */
		// if (is_page('registro')){
		// }
		do_action( 'woocommerce_account_content' );
	?>
</div>
